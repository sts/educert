#!/bin/bash
#Delete pem files from certs folder that are older than 60 minutes
LOCATION="/var/www/educert.cern.ch/certs/"
REQUIRED_FILES="*.pem"
MINUTES=60

find $LOCATION -name $REQUIRED_FILES -type f -mmin +$MINUTES -delete

echo "Deleted files older than $MINUTES minutes"
