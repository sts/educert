<?php
/*
 *  Copyright 2016 CERN
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

// Function to get the SAML token from the local SSO installation
function getToken() {
$assertion=false;
        //Get SAML assertion, either from Mellon
        if(isset($_SERVER['MELLON_SAML_RESPONSE'])) {
                $assertion = base64_decode($_SERVER['MELLON_SAML_RESPONSE']);
                if(!$assertion) {
                        echo "<error>Failed to fetch an assertion from Mellon</error>";
                        header('HTTP/1.1 401 Unauthorized', true, 401);
                }
                
                //Include saml-php library to decrypt assertion, https://github.com/onelogin/php-saml
                define("TOOLKIT_PATH", '/var/www/html/php-saml-2.10.3/');
                require_once(TOOLKIT_PATH . '_toolkit_loader.php');
                
                //Decrypt the assertion, Mellon is issuing encrypted tokens
                $domAssertionEnc = new DOMDocument();
                $domAssertionEnc->preserveWhiteSpace=false;
                $domAssertionEnc->loadXML($assertion);
                $encryptedAssertionEncNodes = $domAssertionEnc->getElementsByTagName('EncryptedAssertion');
                $encryptedAssertionEncNode = $encryptedAssertionEncNodes->item(0);
                $encryptedDataAssertNodes = $encryptedAssertionEncNode->getElementsByTagName('EncryptedData');
                $encryptedDataAssert = $encryptedDataAssertNodes->item(0);

                //Read the VM's private key into an object
                $key = file_get_contents('/etc/certs/educert.cern.ch.key');
                $seckey = new XMLSecurityKey(XMLSecurityKey::RSA_1_5, array('type'=>'private'));
                $seckey->loadKey($key);

                //Use onelogin php-saml library to decrypt
                $decryptedAssertion = OneLogin_Saml2_Utils::decryptElement($encryptedDataAssert, $seckey);
                //Re-parse to remove nasty new line characters
                $checkass = new DomDocument();
                $checkass->preserveWhiteSpace=false;
                $checkass->loadXML($decryptedAssertion->ownerDocument->saveXML($decryptedAssertion));
                $assertion = $checkass->saveXML($checkass->documentElement);

        } else {
                $assertion = false;
        }
        error_log($assertion);
        return $assertion;

}

// Helper function
function customError($reason, $code){
	echo "<error>".$reason."</error>";
	header('HTTP/1.1 '.$code.' '.$reason, true, $code);
        error_log($reason);
}

// Function to check the token and return a sanitised copy to use
// in the soap request
function checkToken($token) {
        $success=true;
	if(!$token) {
	    customError("You are not logged into CERN SSO", 401);
            $success=false;
        }
	$saml = simplexml_load_string($token);
	$saml->registerXPathNamespace('saml2', 'urn:oasis:names:tc:SAML:2.0:assertion');

	date_default_timezone_set('Europe/Berlin');
	$local_time = new DateTime();

	//Check assertion is valid, check time of issue 
	$confirmation_data=$saml->xpath('///saml2:SubjectConfirmationData');
	if (!$confirmation_data) {
		customError("BadlyFormattedSAML", 401);
            	$success=false;
	}
	else {
		$expiry_date=date_create_from_format('Y-m-d\TH:i:s.u\Z',$confirmation_data[0]['NotOnOrAfter'],new DateTimeZone('UTC'));
		if ($local_time > $expiry_date ) {
			customError("Assertion timed out, please authenticate again.", 401);
            		$success=false;
		}
	}
        return $success;
}

function getCertificate($assert, $vo, $configxml) {	
        $success=true;
	$sts_host=$configxml->$vo->stsAddress;
	//Got an SAML Assertion!
	if($assert) {
                $assertxml=simplexml_load_string($assert);
		date_default_timezone_set('Europe/Berlin');
        	$local_time = new DateTime();

		//Create SOAP request for STS
		$request = '<?xml version="1.0" encoding="UTF-8"?>
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Header xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">
		http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wsa:Action>
		<wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">
		urn:uuid:99999999-0000-0000-0000-000000000000</wsa:MessageID>
		<wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">
		<!-- STS host goes here -->'.$sts_host.'</wsa:To>
		<sbf:Framework version="2.0" xmlns:sbf="urn:liberty:sb"/>
		<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
		<wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
		<wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
		<!-- Timestamp goes here -->'.$local_time->format(DateTime::ATOM).'
		</wsu:Created>
		</wsu:Timestamp>
		<!--SAML Assertion goes here-->'.$assert.'
		</wsse:Security>
		</soap:Header>
		<soap:Body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<wst:RequestSecurityToken Context="urn:uuid:00000000-0000-0000-0000-000000000000" xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
		<wst:RequestType xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wst:RequestType>
		<wst:TokenType xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">urn:glite.org:sts:GridProxy</wst:TokenType>
		<wst:Claims Dialect="http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0" xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
		<wsse:SecurityTokenReference xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
		<wsse:Reference URI="#'.$assertxml['ID'].'" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
		</wsse:SecurityTokenReference>
		</wst:Claims>
		<!-- Optional Public Key goes here -->
		<!-- VO goes here -->
		<gridProxy:GridProxyRequest xmlns:gridProxy="urn:glite.org:sts:proxy" lifetime="86400">
		<gridProxy:VomsAttributeCertificates xmlns:gridProxy="urn:glite.org:sts:proxy">
		<gridProxy:FQAN xmlns:gridProxy="urn:glite.org:sts:proxy">'.$vo.':/'.$vo.'</gridProxy:FQAN>
		</gridProxy:VomsAttributeCertificates>
		</gridProxy:GridProxyRequest>
		</wst:RequestSecurityToken>
		</soap:Body>
		</soap:Envelope>';
		
		//Send to STS
		$stscurl = curl_init($sts_host);
		curl_setopt($stscurl, CURLOPT_POST, true);
		//Send our Soap request
		curl_setopt($stscurl, CURLOPT_POSTFIELDS, $request); 
		//Get the output rather than dumping it to screen
		curl_setopt($stscurl, CURLOPT_RETURNTRANSFER, true); 
		//Some certificate issues, would be better not to need this...
		curl_setopt($stscurl, CURLOPT_SSL_VERIFYPEER, false); 
		//Send the correct headers for STS SOAP Action
		curl_setopt($stscurl, CURLOPT_HTTPHEADER, 
		    array ( 'Content-Type : text/xml; charset=utf-8', 'SOAPAction : '.$sts_host ));
		    
		$stsresponse = curl_exec($stscurl);
		if(!$stsresponse) {
			customError("Failed to fetch response from STS: ".htmlspecialchars(curl_error($stscurl)), 503);
                        $success=false;
		} else {
			error_log($stsresponse);
			curl_close($stscurl);
			$success=writeFile($stsresponse, $configxml);
			
		}
	} else {
		customError("You are not logged into CERN SSO", 401);
		$success=false;
	}
	return $success;
}

function writeFile($stsresponse, $configxml) {
        $success=true;

	//Get response from STS and write certificate to file
	//Pick out the Proxy, Cert and Key from the response
	$cert='';
	$proxy='';
	$asymmetric_key='';
	$dom=new DomDocument;
	$dom->loadXML($stsresponse);
	$tokens=$dom->getElementsByTagName('BinarySecurityToken');
	foreach ($tokens as $token){
		$id=$token->getAttribute('wsu:Id');
		if ($id =='#X509SecurityToken' ){ $cert=$token->nodeValue ;}
		elseif ($id =='#VOMSSecurityToken'){$proxy=$token->nodeValue ;}
		//else continue
	}
	$keys=$dom->getElementsByTagName('BinarySecret');
	//There should only be one key. If not, die
	if ($keys->length == 0 ) { 
		customError("User is not registered in VOMS", 503); 
		$succes=false;
	}
	else if ($keys->length != 1 ) { 
		customError("Multiple keys returned from STS", 503); 
		$succes=false;
	}
	else {
		foreach ($keys as $key ){
			$assymetric_key=$key->nodeValue;
		}
		//Check that all response parts are there
		if ($proxy == '' or $cert == '' or $assymetric_key == '') {
			customError("Invalid STS Response", 401);
			$success=false;
		} else {
			//Format file
			$txt="-----BEGIN CERTIFICATE-----\r\n".wordwrap(str_replace("\n","",$proxy),64,"\r\n",true).
			"\r\n-----END CERTIFICATE-----\r\n-----BEGIN PRIVATE KEY-----\r\n".
			wordwrap($assymetric_key,64,"\r\n",true).
			"\r\n-----END PRIVATE KEY-----\r\n-----BEGIN CERTIFICATE-----\r\n".
			wordwrap(str_replace("\n","",$cert),64,"\r\n",true)."\r\n-----END CERTIFICATE-----\r\n";

			//Create a random name for the file
			$file_name = md5( uniqid( rand(), true )).".pem";
			//Save the contents in the file
			$myfile = fopen("/var/www/educert/".$configxml->certDir."/".$file_name, "w") or die ('Cannot open file');
			fwrite($myfile, $txt);
			fclose($myfile);
			//Return the URL of the file
			echo "https://educert.cern.ch/".$configxml->certDir."/".$file_name;
		}
	}
	return $success;
}

//Make sure that the VO is one we are expecting, as listed in the config
function checkVO($vo, $configxml) {
	$success=false;
	$vos = $configxml->voList->vo;
	$vo_count=count($vos);
	for($i = 0; $i < $vo_count; $i++ ){
		if ($vo = $vos[$i] ) { $success=true; }	
	} 
	error_log($valid_vos);
	return $success;
}	

//Read config xml
$configxml=simplexml_load_file("conf/config.xml") or die("Error: Cannot create object");
//Get the assertion and then the certificate
$vo=$_POST['VO'];
if ( checkVO($vo,$configxml) ) {
	$saml_token=getToken();
	if ( $saml_token ) {
        	if ( checkToken($saml_token) ) {
			getCertificate($saml_token, $vo, $configxml);
		}
	}
} else { 
	customError("Invalid VO: ".$vo,401);
}

?>
