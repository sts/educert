**Educert**

Educert, the eduGAIN Certificate Service for WLCG, take the SAML assertion 
from a user's Single-Sign-On (SSO) session, matches it against VOMS and 
produces a VOMS Signed IOTA Certificate Proxy. The certificate is stored on the 
server and a unique URL is returned to the user. This certificate file is 
removed after 1 hour, via a cron job. 

Services contacted:
* Shibboleth SAML endpoint, to retrieve the SAML Assertion
* Security Token Service (STS), to translate the SAML Assertion to x.509
for WLCG Access
