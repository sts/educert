
/* Change the chosen VO
 * */
function changeVO(obj){
	var vo=obj.innerHTML;
	document.getElementById("chosenVO").innerHTML=vo; 
	sessionStorage.chosenVO=vo;
	//Make the go button active, rather than disabled
	document.getElementById("go-button").className = "btn pull-right";
	document.getElementById("go-button").focus();
}
