/* Function to set the loading spinner on the "spinner" element
 and return a handle
*/

function setSpinner(){
	var opts = {
		  lines: 12             // The number of lines to draw
		, length: 7             // The length of each line
		, width: 5              // The line thickness
		, radius: 10            // The radius of the inner circle
		, scale: 1.0            // Scales overall size of the spinner
		, corners: 1            // Roundness (0..1)
		, color: '#000'         // #rgb or #rrggbb
		, opacity: 1/4          // Opacity of the lines
		, rotate: 0             // Rotation offset
		, direction: 1          // 1: clockwise, -1: counterclockwise
		, speed: 1              // Rounds per second
		, trail: 100            // Afterglow percentage
		, fps: 20               // Frames per second when using setTimeout()
		, zIndex: 2e9           // Use a high z-index by default
		, className: 'spinner'  // CSS class to assign to the element
		, top: '50%'            // center vertically
		, left: '50%'           // center horizontally
		, shadow: false         // Whether to render a shadow
		, hwaccel: false        // Whether to use hardware acceleration (might be buggy)
		, position: 'absolute'  // Element positioning
	}
	var target = document.getElementById('spinner')
	var spinner = new Spinner(opts).spin(target)
	return spinner;
}

/* Function to get the certificate through the PHP method accessing both 
Shibboleth and STS on the backend and returning a URL to the generated file
*/
function getCertificate(){
	console.log("Getting certificate for: "+sessionStorage.chosenVO);

	var spinner = setSpinner();
	$.ajax({
		type: "POST",
                url: "get_cert.php",
		data: { VO: sessionStorage.chosenVO },
		success : function(result, status, xhr) {
			//Certificate has probably been generated
			console.log(result);
			
			var certdiv = document.getElementById("certificate");
                        certdiv.innerHTML = result;
                        certdiv.style.display="block";		
                        document.getElementById('copy-button').style.visibility = 'inherit';
			document.getElementById('copy-button').focus();
                        showAlert("Certificate Generated!", "alert-success");
			spinner.stop();
			
		},
		error : function(xhr, status, errormsg) {
			console.log(errormsg +" "+ status + " " + JSON.stringify(xhr))
			var text = xhr.statusText;
			showAlert(text, "alert-danger");
			spinner.stop();
		}
	});

}


/* Helper function to show a short lived alert
*/
function showAlert(message,alerttype) {
    	$('#alert_placeholder').append('<div id="alertdiv" class="alert ' 
			+  alerttype + '"><a class="close" data-dismiss="alert" aria-label="close">&times;</a><span>'
			+message+'</span></div>');

    	setTimeout(function() { // this will automatically close the alert and remove this if the users doesnt close it in 5 secs
		$("#alertdiv").remove();
    	}, 5000);
	
};

