<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title>Educert</title>

<!-- Scripts for educert -->
<script src="js/util.js"></script> 
<script src="js/certificate.js"></script>
<script src="js/spin.js"></script>

<!-- Bootstrap Css --!>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!--Boostrap Css Overrides -->
<link rel="stylesheet" type="text/css" href="css/style.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https:///maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!-- Clipboard -->
<script src="https://cdn.jsdelivr.net/clipboard.js/1.5.10/clipboard.min.js"></script>

</head>

<body >
	<!-- Nav bar -->
	<div class="row">
	  <div class="col-md-12">
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="https://educert.cern.ch/">Educert</a>
		    </div>
		    <ul class="nav navbar-nav navbar-right">
	      	      <li><a>You are logged in as <?php echo $_SERVER['REMOTE_USER'] ?></a></li>
		      <li><a href="#helpModal" data-toggle="modal">Help</a></li>
		      <li><a href="https://login.cern.ch/adfs/ls/?wa=wsignout1.0"><span class="glyphicon glyphicon-log-in"></span> LogOut</a></li> 
		    </ul>
		  </div>
		</nav>
	   </div>
	  </div>
	<!-- Header -->
	  <div class="row">
          <div class="col-md-12">
	    <div class="container">
		<h1>Educert</h1>
		<p>Educert creates short-lived VOMS Proxy Certificates to enable access to WLCG resources.
			Generate a certificate and save the file to your local PC.<br><br>Your login account must already be registered by your VO.</p>
	    </div>
	  </div>
	</div>
	<!-- Buttons -->
	<div class="row">
          <div class="col-md-12">
            <div class="container">
		<div class="dropdown">
		  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" id="chosenVO" >Choose your VO...
		  <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<?php 
			$configxml=simplexml_load_file("conf/config.xml") or die("Error: Cannot create object");
			$vo_count=count($configxml->voList->vo);
			for($i = 0; $i < $vo_count; $i++ ){
				echo "<li><a onclick=\"changeVO(this); return false;\" href=\"#\">".$configxml->voList->vo[$i]."</a></li>";
			}
			?>		
		  </ul>
		</div>
	    </div>
          </div>
	</div>
        <div class="row">
          <div class="col-md-12">
		<div class="container">
			<button onclick="getCertificate()" id="go-button" class="btn pull-right disabled">Get Certificate</button>
                        <button data-clipboard-target="#certificate" class="btn pull-right btn-primary" id="copy-button" style="visibility:hidden">Copy</button>
		</div>
	    <!-- Spinner -->
            <div id="spinner"></div>
          </div>
        </div>
	<hr>
	<!-- Alerts -->
	<div class="row">
          <div class="col-md-12">
		<div class="container" id="alert_placeholder"></div>
	  </div>
	</div>
	<!-- Certificate --> 
	<div class="row">
          	<div class="col-md-6 col-md-offset-3">
				<div class="well well-lg" id="certificate" ></div>
		</div>
	</div>
	<script>
	    var clipboard = new Clipboard('#copy-button');
	    clipboard.on('success', function(e) {
		console.log(e);
	    });
	    clipboard.on('error', function(e) {
		console.log(e);
	    });
	</script>


	<!-- Help Modal -->
	<div id="helpModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Need help?</h4>
	      </div>
	      <div class="modal-body">
		<h4>Cannot generate a certificate?</h4>
		<p>You must already be a registered user of a WLCG VO. Follow your VO's procedures to register your account.
			If you are already registered, try logging out and in again.</p>
		<br>
		<h4>What should I do with my certificate?</h4>
		<p>Your VOMS Proxy certificate should be saved at the location indicated by your environment variable 
			X509_USER_PROXY. To list your environment variables, use the printenv command in your console.</p>
		<br>
	      	<h4>How do I download my certificate?</h4>
		<p>From the command line, use curl or wget to download the certificate from your certificate URL.</p>
		</div>
	      <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

</body>
</html>

